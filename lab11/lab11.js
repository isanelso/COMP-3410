class lab11{
  testDefaultParameter(param1, param2=100){
  return  {
      'first': param1,
      'second': param2
      };
  };
  testTemplateLiterals(firstname, middlename, lastname){
    return `${firstname} ${middlname} ${lastname}`;
  };
  testMultilineStrings(){
    return `wow a multi line string?
    no way!
    no way!
    no way!`;
  };
  testSortWithArrowFunction(array){
  array.sort((a,b)=>{
    if(a==b){
      return 0;
    }
    else if(a<b){
      return 1;
    }
    else{
      return -1;
    }
  });
  return array;
}

let variable = mylab11.testDefaultParameter()

export class lab11;
